from __future__ import print_function
import sys
import os
import pickle
from pybrain.tools.shortcuts import buildNetwork
from pybrain.datasets import SupervisedDataSet
from pybrain.supervised.trainers import BackpropTrainer
from PIL import Image


def help_and_exit():
    print('Required arguments:\n'
          ' 1. -t for training, -r for recognition\n'
          ' 2. Path to directory with images\n'
          ' 3. Filename for network opening (-r) or [Max number of epochs (-t)]\n'
          ' [4. Min learning speed (-t)] or [-v for verbose statistic]\n'
          ' [5. Filename for network saving (-t)]')
    exit()


def load(folder):
    result = []
    paths = [file for file in os.listdir(folder) if file.endswith('.png')]
    for path in paths:
        image = Image.open('{0}/{1}'.format(folder, path))
        pixels = image.load()
        digit = []
        for i in range(image.size[1]):
            for j in range(image.size[0]):
                if pixels[j, i][0] == 0:
                    digit.append(0)
                else:
                    digit.append(1)
        output = [0 for i in range(10)]
        output[int(path[0])] = 1
        result.append([digit, output, path])
    return result


def train():
    ds = SupervisedDataSet(35, 10)

    for ioset in load(sys.argv[2]):
        ds.addSample(ioset[0], ioset[1])

    net = buildNetwork(35, 50, 10)
    trainer = BackpropTrainer(net, ds)

    min_speed = float(sys.argv[4])
    epochs = int(sys.argv[3])
    for i in range(epochs):
        speed = trainer.train()
        if i % 10 == 0:
            print(speed)
        if speed < min_speed:
            print(speed)
            break

    file = open(sys.argv[5], 'wb')
    pickle.dump(net, file, pickle.HIGHEST_PROTOCOL)
    file.close()


def recognize():
    file = open(sys.argv[3], 'rb')
    net = pickle.load(file)

    correct = 0
    incorrect = 0
    for ioset in load(sys.argv[2]):
        result = list(net.activate(ioset[0]))

        expectation = ioset[1].index(max(ioset[1]))
        reality = result.index(max(result))
        if expectation == reality:
            correct += 1
        else:
            incorrect += 1
        if len(sys.argv) == 5 and sys.argv[4] == '-v':
            print('Image: {0} | Expectation: {1} | Reality: {2}'.format(ioset[2], expectation, reality))
    print('Correct: {0} | Incorrect: {1} | Percent: {2}'.format(correct, incorrect,
                                                                (float(correct) / float(correct + incorrect)) * 100))


if __name__ == '__main__':
    if len(sys.argv) < 3:
        help_and_exit()

    if sys.argv[1] == '-t' and len(sys.argv) < 6:
        help_and_exit()
    elif sys.argv[1] == '-r' and len(sys.argv) < 4:
        help_and_exit()

    if sys.argv[1] == '-t':
        train()

    if sys.argv[1] == '-r':
        recognize()
