# README #

Neural network for recognition of 5x7 digit images.

## How to run ##

### Training mode ###

python net.py -t <images> <epochs> <speed> <file>

* -t - Training flag
* <images> - Path to directory with images
* <epochs> - Max number of epochs
* <speed> - Min learning speed
* <file> - File to save trained network

Sample:

* python net.py -t train 1000 0.001 data.pkl

### Training mode ###

python net.py -t <images> <file> [-v]

* -r - Recognition flag
* <images> - Path to directory with images
* <file> - File with trained network
* -v - For verbose log

Sample:

* python net.py -r test data.pkl -v